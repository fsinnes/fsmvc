﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FSMVC.Models
{
    public class TicketStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
