﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FSMVC.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime CheckIn { get; set; }
        public string CheckInNotes { get; set; }
        public TicketStatus TicketStatus { get; set; }
        public int TicketStatusId { get; set; }
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }
    }
}
