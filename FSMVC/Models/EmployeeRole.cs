﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FSMVC.Models
{
    public class EmployeeRole
    {
        public int Id { get; set; }
        public string Role { get; set; }
    }
}
