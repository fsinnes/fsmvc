﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FSMVC.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternatePhone { get; set; }
        public string EmailAddress { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public EmployeeRole EmployeeRole { get; set; }
        public int EmployeeRoleId { get; set; }
    }
}
